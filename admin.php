<?php
	include("inc_inti.php");
	include("inc_dbconnect.php");

	$str_condtion = "";
	$str_kw = "";

	if( isset($_GET['kw']) ) {
		$str_kw = $_GET['kw'];
		$str_condtion = " Where product_name like '%" . $str_kw . "%'";
		$str_condtion .= " or tracking_id like '%" . $str_kw . "%'";
	}

	$str_sql = "select * FROM fixproduct_tb". $str_condtion;
	$obj_rs = mysqli_query($obj_con,$str_sql);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan+2|Prompt&display=swap" rel="stylesheet">
</head>
<body>
	<header>
		<a href="service.php"><img src="images/home.png"></a>
		<h1>Repair Service</h1>
	</header>

	<div class="container">
		<div class="admin">
			<div class="left"> ADMIN</div>
			<div class="right">
				<form name="frmdata" method="post" action="r_login.php">
					<input type="submit" name="btnlogout" value="Logout">
				</form>
			</div>
			<div class="clear"></div>
		</div>

		<div class="main">
			<div class="left">
				<form name="frmdata" method="post" action="r_manager.php">
					<label for="find">ค้นหาจากชื่อสินค้า หรือ tracking_id </label>
					<input type="text" name="txtkw">
					<input type="submit" name="find" value="ค้นหา">
				</form>
			</div>

			<div class="right">
				<form name="frmdata" method="post" action="inform.php">
						<input type="submit" name="subadd" value="เพิ่ม">			
				</form>
			</div>
			<div class="clear"></div>
		</div>
		

		<table class="information">
			<tr>
				<th>สินค้าที่นำมาซ่อม</th>
				<th>สถานที่รับสินค้าแจ้งซ่อม</th>
				<th>วันที่และเวลา นัดรับของแจ้งซ่อม</th>
				<th>เบอร์โทรติดต่อกลับ</th>
				<th>สถานที่ ส่งของ เมื่อซ่อมเสร็จ</th>
				<th>อาการ</th>
				<th>tracking_id</th>
				<th>status</th>	
				<th>การจัดการ</th>
			</tr>
		<?php while ($obj_row = mysqli_fetch_array($obj_rs) ) { ?>	
			<tr>
				<td><?= $obj_row['product_name']?></td>
				<td><?= $obj_row['place_recieve']?></td>
				<td><?= $obj_row['datetime_recieve']?></td>
				<td><?= $obj_row['phone_number']?></td>
				<td><?= $obj_row['place_send']?></td>
				<td><?= $obj_row['detail']?></td>
				<td><?= $obj_row['tracking_id']?></td>
				<td><?= $obj_row['status']?></td>
				<td>
					<div class="manage">
							<form name="frmdata" method="post" action="r_manager.php">
								<input type="submit" name="subedit" value="แก้ไข">	
								<input type="submit" name="subdel" value="ลบ">
								<input type="hidden" name="hdid" value="<?= $obj_row['user_id']?>">
							</form>
					</div>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>

</body>
</html>