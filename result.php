<?php
    session_start();

	include("inc_dbconnect.php");
	
	if (isset($_SESSION['tracking_id'])){

       $tracking_id = $_SESSION['tracking_id'];

       $str_sql = "select * FROM fixproduct_tb WHERE tracking_id ='".$tracking_id."'";
       // $str_sql1 = "select * FROM fixproduct_tb WHERE tracking_id =" . $tracking_id;

    }else{

    	$str_sql = "select * FROM fixproduct_tb where";
    }

	$obj_rs = mysqli_query($obj_con,$str_sql);

	if( !$obj_row = mysqli_fetch_array($obj_rs) ) {
	header("location:inform.php");
	exit;
	}
	session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
	<title>ANNIE'S Service and delivery</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan+2|Prompt&display=swap" rel="stylesheet">
</head>
<body>
	<header>
		<a href="service.php"><img src="images/home.png"></a>
		<h1>Repair Service</h1>
	</header>
	<div class="container">
		<h2>รายละเอียดการแจ้งซ่อม</h2><br>	

		<table class="result">
			<tr>
				<th>สินค้าที่นำมาซ่อม</th>
				<td><?= $obj_row['product_name']?></td>
			</tr>
			<tr>
				<th>สถานที่ นัดรับ ของแจ้งซ่อม</th>
				<td><?= $obj_row['place_recieve']?></td>
			</tr>
			<tr>
				<th>วันที่และเวลา นัดรับของแจ้งซ่อม</th>
				<td><?= $obj_row['datetime_recieve']?></td>
			</tr>
			<tr>
				<th>สถานที่ ส่งของ เมื่อซ่อมเสร็จ</th>
				<td><?= $obj_row['place_send']?></td>
			</tr>
			<tr>
				<th>เบอร์โทรติดต่อกลับ</th>
				<td><?= $obj_row['phone_number']?></td>
			</tr>
			<tr>
				<th>อาการ และ รายละเอียดเพิ่มเติม</th>
				<td><?= $obj_row['detail']?></td>
			</tr>
			<tr  class="tracking">
				
					<th>รหัสเช็คสถานะ</th>
					<td><?= $obj_row['tracking_id']?></td>
				
			</tr>
		</table>
		<br>
		<p>*โปรดแคปหน้าจอเพื่อดูรายละเอียดและรหัสเช็คสถานะ เพื่อใช้ตรวจสอบสถานะของสินค้า<p><br><br>
		<!-- <a href="service.php">กลับหน้าแรก</a><br> -->
		


	

	</div>
	


	
</body>
</html>