<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan+2|Prompt&display=swap" rel="stylesheet">
</head>
<body>
<header>
		<a href="service.php"><img src="images/home.png"></a>
		<h1>Repair Service</h1>
	</header>

	<div class="container">
		<h2>แบบฟอร์มแจ้งซ่อม</h2>

		<form name="data" method="post" action="adddata.php">
			<label>สินค้าที่นำมาซ่อม</label>
			<input type="text" name="productname" required><br>

			<label>สถานที่ นัดรับ ของแจ้งซ่อม</label>
			<input type="text" name="place-recieve" required><br>

			<label>วันที่และเวลา นัดรับของแจ้งซ่อม</label>
			<input type="datetime-local" name="datetime" required><br>

			<label>สถานที่ ส่งของ เมื่อซ่อมเสร็จ</label>
			<input type="text" name="place-send" required><br>

			<label>เบอร์โทรติดต่อกลับ</label>
			<input type="tel" name="phone" placeholder="08x-xxx-xxxx" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required ><br>

			<!-- <label>รหัสเช็คสถานะ</label>
			<input type="text" name="status"><br> -->

			<label>อาการ และ รายละเอียดเพิ่มเติม</label><br>
			<textarea name="detail" rows="10" cols="50" required>ระบุอาการที่พบ...</textarea><br>

			<input type="submit" name="submit">
		</form>
	</div>
	


	
</body>
</html>