<?php
	session_start();

	if(isset($_SESSION['user']['id'])) {
		header("location:admin.php");
		exit;
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan+2|Prompt&display=swap" rel="stylesheet">
</head>
<body>
	<div class="container loginpage">
		<h2>Welcome Back</h2>
		<p>Sign in to your account</p>

		<form method="post" name="login" action="r_login.php">
			<input type="text" placeholder="Username" name="txtusername" required><br>
			<input type="password" placeholder="Password" name="txtpassword" required><br>
			<input type="submit" name="btnlogin" value="Login" >
		</form>
	</div>
</body>
</html>