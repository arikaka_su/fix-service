<?php
	include("inc_inti.php");
	$A=$_GET['id'];

	include("inc_dbconnect.php");

	$str_sql = "select * FROM fixproduct_tb where user_id =" . $A;
	$obj_rs = mysqli_query($obj_con,$str_sql);

	if(!$obj_row = mysqli_fetch_array($obj_rs) ) {
		header("location:admin.php");
		exit;
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan+2|Prompt&display=swap" rel="stylesheet">
</head>
<body>
	<header>
		<a href="service.php"><img src="images/home.png"></a>
		<h1>Repair Service</h1>
	</header>

	<div class="container">
		<h2>แก้ไขข้อมูล</h2>

		<form name="frmdata" method="post" action="saveedit.php" enctype="multipart/form-data">
			<label>สินค้าที่นำมาซ่อม</label> 
			<input type="text" name="product_name" value="<?= $obj_row['product_name']?>"><br>

			<label>สถานที่รับสินค้าแจ้งซ่อม</label>
			<input type="text" name="place_recieve" value="<?= $obj_row['place_recieve']?>"><br>

			<label>วันที่และเวลา นัดรับของแจ้งซ่อม</label>
			<input type="text" name="datetime_recieve" value="<?= $obj_row['datetime_recieve']?>"><br>

			<label>เบอร์โทรติดต่อกลับ</label>
			<input type="text" name="phone_number" value="<?= $obj_row['phone_number']?>"><br>

			<label>สถานที่ ส่งของ เมื่อซ่อมเสร็จ</label>
			<input type="text" name="place_send" value="<?= $obj_row['place_send']?>"><br>

			<label>อาการ</label>
			<input type="text" name="detail" value="<?= $obj_row['detail']?>"><br>

			<label>tracking_id</label>
			<input type="text" name="tracking_id" value="<?= $obj_row['tracking_id']?>"><br><br>

			<label><b>status</b></label><br>
            <?php if ($obj_row['status'] == "inprocess"):?>
			<input type="radio" name="status" value="inprocess" checked="checked">
			<?php elseif ($obj_row['status'] != "inprocess"):?>
			<input type="radio" name="status" value="inprocess">
			<?php endif; ?>
			<label for="inprocess">ระหว่างดำเนินการ</label><br>

			<?php if ($obj_row['status'] == "fixed"):?>
			<input type="radio" name="status" value="fixed" checked="checked">
			<?php elseif ($obj_row['status'] != "fixed"):?>
			<input type="radio" name="status" value="fixed">
			<?php endif; ?>
			<label for="fixed">ซ่อมเรียบร้อยแล้วรอรับคืน</label><br>
			
			<?php if ($obj_row['status'] == "cantfix"):?>
			<input type="radio" name="status" value="cantfix" checked="checked">
			<?php elseif ($obj_row['status'] != "cantfix"):?>
			<input type="radio" name="status" value="cantfix">
			<?php endif; ?>
			<label for="cantfix">ไม่สามารถซ่อมได้รอรับคืน</label><br>


			<input type="submit" name="save" value="บันทึก">	
			<input type="submit" name="cancel" value="ยกเลิก">	

			<input type="hidden" name="hdid" value="<?= $obj_row['user_id']?>">			
		</form>
	</div>

	

</body>
</html>