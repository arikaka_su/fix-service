<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan+2|Prompt&display=swap" rel="stylesheet">
</head>
<body>
	<header>
		<a href="service.php"><img src="images/home.png"></a>
		<h1>Repair Service</h1>
	</header>
	<div class="container">
		<div class="inform box">
			<a href="inform.php"><img src="images/fix.jpg"> แจ้งซ่อม</a>
		</div>
		<div class="status box">
			<a href="status.php"><img src="images/check.jpg"> ตรวจสอบสถานะ</a>
		</div>
	</div>
	
</body>
</html>