<?php
	if( isset($_POST['subadd']) ) {
		header("location:adddata.php");

	} elseif( isset($_POST['subedit']) ) {
		$str_id = $_POST['hdid'];
	 	header("location:edit.php?id=" . $str_id);

	} elseif( isset($_POST['subdel']) ) {
		$str_id = $_POST['hdid'];
	 	header("location:delete.php?id=" . $str_id);

	} elseif( isset($_POST['find']) ) {
		$str_kw = $_POST['txtkw'];
		$str_qs = "";
		if(strlen($str_kw) > 3) {
			$str_qs = "?kw=" . $str_kw; 
		}

	 	header("location:admin.php" . $str_qs);
	} else {
		header("location:admin.php");
	}
	exit();
?>